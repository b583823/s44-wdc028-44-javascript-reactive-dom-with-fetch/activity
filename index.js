// Scenario: Let's create a simple program that will perform CRUD operation for posting the available movies to watch.

// Mock Database

let posts = [];

let count = 1;

// Get post data
fetch("https://jsonplaceholder.typicode.com/posts")
.then((response) => response.json())
.then((data)=>showPosts(data));
// Add post data.
document.querySelector("#form-add-post").addEventListener("submit", (e)=>{
	// Prevents the page from loading
	e.preventDefault();
	fetch("https://jsonplaceholder.typicode.com/posts",{
		method: "POST",
		body: JSON.stringify({
			title: document.querySelector("#txt-title").value,
			body: document.querySelector("#txt-body").value,
			userId: 1
		}),
		headers: {
			"Content-type": "application/json"
		}
	})
	.then((response)=>response.json())
	.then((data=>{
		console.log(data);
		alert("Successfully added!");
	}))
	document.querySelector("#txt-title").value = null;
	document.querySelector("#txt-body").value = null;

	
})

// View Posts

const showPosts = (posts) =>{
	let postEntries = "";
	// We will used forEach() to display each movie inside our mock database.
	posts.forEach((post)=>{
	postEntries += `
	<div id="post-${post.id}">
		<h3 id="post-title-${post.id}">${post.title}</h3>
		<p id="post-body-${post.id}">${post.body}</p>
		<button onclick="editPost('${post.id}')">Edit</button>
		<button onclick="deletePost('${post.id}')">Delete</button>
		</div>
	`
	});
	// console.log(postEntries)
	document.querySelector("#div-post-entries").innerHTML = postEntries;
}

// Edit Post button
// We will create a function that will be called in the conlick() event and will pass the value in the Update Form input box.

const editPost = (id) =>{
	// Contain the value of the title and body in a variable.
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;
		// Pass the id, title, and body of movie post to be updated in the Edit Post/Form.
		document.querySelector("#txt-edit-id").value = id;
		document.querySelector("#txt-edit-title").value = title;
		document.querySelector("#txt-edit-body").value = body;
		document.querySelector("#btn-submit-update").removeAttribute("disabled")
}

// Update post.

document.querySelector("#form-edit-post").addEventListener("submit", (e)=>{
	e.preventDefault();
	let id = document.querySelector("#txt-edit-id").value
	fetch(`https://jsonplaceholder.typicode.com/posts/${id}`,{
		method: "PUT",
		body: JSON.stringify({
			id: id,
			title: document.querySelector("#txt-edit-title").value,
			body: document.querySelector("#txt-edit-body").value,
			userId: 1
		}),
		headers: {
			"Content-Type" : "application/json"
		}
	})
	.then(response => response.json())
	.then(data =>{
		console.log(data);
		alert("Successfully Updated")
		document.querySelector("txt-edit-title").value=null;
		document.querySelector("txt-edit-body").value=null;
		// Add a attribute in a HTML element
		document.querySelector("#btn-submit-update").setAttribute("disabled",true)
	})
})

// Delete post..
const deletePost = (id) =>{
	let index = posts.indexOf(id)
	console.log(index);
	posts.splice(id-1,1);
	alert("Successfully updated!");
	showPosts()
}